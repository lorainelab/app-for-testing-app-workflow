Allows testing of IGB App installation and update workflow. 

To run App for Testing App Workflow:

1. Install the App.
2. Select **App for Testing App Workflow** under **Tools** menu. 
3. Observe that a message dialog window appears on the screen.
